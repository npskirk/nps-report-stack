# `nps-latex`

A docker image for writing Technical Reports and Theses using LaTeX and the NPS LaTeX report class.

## Background

* Installing and setting up LaTeX is a royal pain on any platform.
* Maintaining how-to's for many platforms is a pain
* Copying the raw `report.cls` file for every thesis means every thesis is using a different `report.cls` file.
* Keeping the thesis template project in a github repository encourages adding personal stuff (like bib files) that nobody else cares about to the template project.  The github repo is a great idea, but it doesn't help much with keeping your thesis up to date with the template as it gets updated.

Docker can help with this in the following ways.

* Docker is easy to set up on all platforms
* Once docker is installed, you can just use this container as the place from which you run LaTeX and associated tools.
* To change which version of the class and bib styles you're using, just use the docker image with the version you want instead of trying to merge and manage the class and style files.
* We can, over time, evolve the whole typsetting stack in this container without making people follow a bunch of manual installation and configuration instructions.

## Using

This container lets you ignore the installation and configuration of LaTeX, but you still need a main `.tex` source file that is set up to use the nps `report.cls` and associated things.
Any of the examples in the examples folder could serve as a starting point.

In your thesis or report directory (the one with your main `.tex` document), type

    docker run -v $(pwd):/source -t kastork/nps-latex /bin/bash

This puts you into a command line with your project as the current directory.
The tools of this container are now available to you. Typically, you would type something like this to cause the tools to typeset your document every time you save a change.

	latexmk -pdf -f -outdir=./build report

where `report` is referring to a file in your project directory called `report.tex`.
Obviously, you can name this file whatever you want, but it must have the `.tex` suffix.

`latexmk` handles all the chores of repeatedly typesetting, re-typsetting, citation processing, re-typesetting and so on until no more passes are needed.
Sometimes this can take a while, and sometimes, it will stop and wait for you to answer some question it has posed.
For info on how to answer those questions, I refer you to the LaTeX documentation.

The files in your project directory are not copied, you can edit and view them in place using your favorite Windows or macOS tools.
